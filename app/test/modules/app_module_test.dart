import 'package:app/controlles/app_controller.dart';
import 'package:app/interfaces/enterprises_respository_interface.dart';
import 'package:app/interfaces/http_client_service_interface.dart';
import 'package:app/modules/app_module.dart';
import 'package:app/repositories/ioasys_enterprise_repository.dart';
import 'package:app/services/dio_http_client_service.dart';
import 'package:app/stores/enterprises_store.dart';
import 'package:app/stores/login_store.dart';
import 'package:app/view_models/enterprises_view_model.dart';
import 'package:app/view_models/login_view_model.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';

main() {
  initModule(AppModule());

  group("App Module (Dependency Injection) Test:", () {
    test("get app controller instance", () {
      final instance = Modular.get<AppController>();
      expect(instance, isA<AppController>());
    });
    test("get login view model instance", () {
      final instance = Modular.get<LoginViewModel>();
      expect(instance, isA<LoginViewModel>());
    });
    test("get enterprises view model instance", () {
      final instance = Modular.get<EnterprisesViewModel>();
      expect(instance, isA<EnterprisesViewModel>());
    });
    test("get ioasys enterprises repository instance", () {
      final instance = Modular.get<IEnterpriseRepository>();
      expect(instance, isA<IoasysEnterpriseRepository>());
    });
    test("get dio http client service instance", () {
      final instance = Modular.get<IHttpClientService>();
      expect(instance, isA<DioHttpClientService>());
    });
    test("get login store instance", () {
      final instance = Modular.get<LoginStore>();
      expect(instance, isA<LoginStore>());
    });
    test("get enterprises store instance", () {
      final instance = Modular.get<EnterprisesStore>();
      expect(instance, isA<EnterprisesStore>());
    });
  });
}
