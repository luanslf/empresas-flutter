import 'package:app/controlles/app_controller.dart';
import 'package:app/models/auth_model.dart';
import 'package:app/models/enterprise_model.dart';
import 'package:app/modules/app_module.dart';
import 'package:app/pages/entrerprise_page.dart';
import 'package:app/pages/home_page.dart';
import 'package:app/pages/login_page.dart';
import 'package:app/pages/root_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';

main() {
  initModule(AppModule());
  AppController appController;
  setUp(() {
    appController = Modular.get<AppController>();
  });
  group("Root Page Test:", () {
    testWidgets("Has a observer widget", (tester) async {
      await tester.pumpWidget(MaterialApp(home: RootPage()));
      expect(find.byType(Observer), findsOneWidget);
    });
    testWidgets("Returns Login Page on init state", (tester) async {
      await tester.pumpWidget(MaterialApp(home: RootPage()));
      expect(find.byType(LoginPage), findsOneWidget);
    });
    testWidgets("Dont returns Home Page on init state", (tester) async {
      await tester.pumpWidget(MaterialApp(home: RootPage()));
      expect(find.byType(HomePage), findsNothing);
    });
    testWidgets("Returns Home Page after set Auth Model", (tester) async {
      await tester.pumpWidget(MaterialApp(home: RootPage()));
      appController.loginViewModel.loginStore.setAuthModel(new AuthModel());
      await tester.pumpAndSettle(Duration(seconds: 1));
      expect(find.byType(HomePage), findsOneWidget);
    });
    testWidgets("Returns Enterprise Page after select a enterprise",
        (tester) async {
      await tester.pumpWidget(MaterialApp(home: RootPage()));
      appController.loginViewModel.loginStore.setAuthModel(new AuthModel());
      await tester.pumpAndSettle(Duration(seconds: 1));
      appController.enterprisesViewModel.enterprisesStore
          .setSelectedEnterpriseModel(
              new EnterpriseModel(enterpriseName: "", description: ""));
      await tester.pumpAndSettle(Duration(seconds: 1));
      expect(find.byType(EnterprisePage), findsOneWidget);
    });
  });
}
