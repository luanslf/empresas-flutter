import 'package:app/controlles/app_controller.dart';
import 'package:app/modules/app_module.dart';
import 'package:app/pages/components/login_button.dart';
import 'package:app/pages/components/login_text_field.dart';
import 'package:app/pages/loading_login_page.dart';
import 'package:app/pages/login_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';

main() {
  initModule(AppModule());
  AppController appController;
  setUp(() {
    appController = Modular.get<AppController>();
  });
  group("Login Page Test:", () {
    testWidgets("has login button widget", (tester) async {
      await tester.pumpWidget(MaterialApp(home: LoginPage()));
      expect(find.byType(LoginButton), findsOneWidget);
    });
    testWidgets("has a custom scroll view widget", (tester) async {
      await tester.pumpWidget(MaterialApp(home: LoginPage()));
      expect(find.byType(CustomScrollView), findsOneWidget);
    });
    testWidgets("dont have a loading login page at init state", (tester) async {
      await tester.pumpWidget(MaterialApp(home: LoginPage()));
      expect(find.byType(LoadingLoginPage), findsNothing);
    });
    testWidgets("has two login text field widgets", (tester) async {
      await tester.pumpWidget(MaterialApp(home: LoginPage()));
      expect(find.byType(LoginTextField), findsNWidgets(2));
    });
  });
}
