import 'package:app/controlles/app_controller.dart';
import 'package:app/models/enterprise_model.dart';
import 'package:app/modules/app_module.dart';
import 'package:app/pages/entrerprise_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';

main() {
  initModule(AppModule());
  AppController appController;
  setUp(() {
    appController = Modular.get<AppController>();
  });
  group("Enterprise Page Test:", () {
    testWidgets("has a observer widget", (tester) async {
      await tester.pumpWidget(MaterialApp(home: EnterprisePage()));
      expect(find.byType(Observer), findsOneWidget);
    });
    testWidgets("returns a container when selected enterprise is null",
        (tester) async {
      await tester.pumpWidget(MaterialApp(home: EnterprisePage()));
      expect(
        find.ancestor(
            of: find.byType(Container), matching: find.byType(Observer)),
        findsOneWidget,
      );
    });
    testWidgets("dont returns a scaffold when selected enterprise is null",
        (tester) async {
      await tester.pumpWidget(MaterialApp(home: EnterprisePage()));
      expect(find.byType(Scaffold), findsNothing);
    });
    testWidgets("returns a scaffold after select a enterprise", (tester) async {
      await tester.pumpWidget(MaterialApp(home: EnterprisePage()));
      final enterpriseModel =
          EnterpriseModel(enterpriseName: "name", description: "description");
      appController.enterprisesViewModel.selectEnterprise(enterpriseModel);
      await tester.pumpAndSettle();
      expect(find.byType(Scaffold), findsOneWidget);
    });
    testWidgets("has two text widgts with enterprise name", (tester) async {
      await tester.pumpWidget(MaterialApp(home: EnterprisePage()));
      final enterpriseModel =
          EnterpriseModel(enterpriseName: "name", description: "description");
      appController.enterprisesViewModel.selectEnterprise(enterpriseModel);
      await tester.pumpAndSettle();
      expect(find.text(enterpriseModel.enterpriseName), findsNWidgets(2));
    });
    testWidgets("has a text widgts with enterprise description",
        (tester) async {
      await tester.pumpWidget(MaterialApp(home: EnterprisePage()));
      final enterpriseModel =
          EnterpriseModel(enterpriseName: "name", description: "description");
      appController.enterprisesViewModel.selectEnterprise(enterpriseModel);
      await tester.pumpAndSettle();
      expect(find.text(enterpriseModel.description), findsOneWidget);
    });
  });
}
