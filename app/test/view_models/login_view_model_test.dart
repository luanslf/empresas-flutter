import 'package:app/interfaces/enterprises_respository_interface.dart';
import 'package:app/models/search_response_model.dart';
import 'package:app/models/search_model.dart';
import 'package:app/models/login_model.dart';
import 'package:app/models/auth_model.dart';
import 'package:app/modules/app_module.dart';
import 'package:app/view_models/login_view_model.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';

class EnterprisesRepositoryMock implements IEnterpriseRepository {
  @override
  Future<SearchResponseModel> search(SearchModel searchModel) {
    throw UnimplementedError();
  }

  @override
  Future<AuthModel> signin(LoginModel loginModel) async {
    await Future.delayed(Duration(seconds: 1));
    if (loginModel == null) return null;
    if (loginModel.email != "valid@email.com" ||
        loginModel.password != "validPassword") {
      return null;
    }
    return AuthModel();
  }
}

main() {
  initModule(
    AppModule(),
    changeBinds: [
      Bind<IEnterpriseRepository>((i) => EnterprisesRepositoryMock())
    ],
  );

  LoginViewModel loginViewModel;
  LoginModel validLoginModel =
      LoginModel(email: "valid@email.com", password: "validPassword");
  LoginModel wrongPasswordLoginModel =
      LoginModel(email: "valid@email.com", password: "invalidPassword");
  LoginModel wrongEmailLoginModel =
      LoginModel(email: "invalid@email.com", password: "validPassword");

  setUp(() {
    loginViewModel = Modular.get<LoginViewModel>();
    loginViewModel.isAuthenticatedOutput
        .listen(loginViewModel.loginStore.setAuthModel);
  });

  group("Login View Model Test:", () {
    test("init auth model is null", () {
      expect(loginViewModel.loginStore.authModel, isNull);
    });
    test("auth model is null after login with wrong email", () async {
      loginViewModel.isAuthenticatedInput.add(wrongEmailLoginModel);
      await Future.delayed(Duration(seconds: 1));
      expect(loginViewModel.loginStore.authModel, isNull);
    });
    test("auth model is null after login with wrong password", () async {
      loginViewModel.isAuthenticatedInput.add(wrongPasswordLoginModel);
      await Future.delayed(Duration(seconds: 1));
      expect(loginViewModel.loginStore.authModel, isNull);
    });
    test("auth model is not null after login with valid credentials", () async {
      loginViewModel.isAuthenticatedInput.add(validLoginModel);
      await Future.delayed(Duration(seconds: 2));
      expect(loginViewModel.loginStore.authModel, isA<AuthModel>());
    });
  });
}
