import 'package:app/interfaces/enterprises_respository_interface.dart';
import 'package:app/models/auth_model.dart';
import 'package:app/models/enterprise_model.dart';
import 'package:app/models/search_response_model.dart';
import 'package:app/models/search_model.dart';
import 'package:app/models/login_model.dart';
import 'package:app/modules/app_module.dart';
import 'package:app/view_models/enterprises_view_model.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';

const String INVALID_UID = "invalidUid";
const String INVALID_ACCESS_TOKEN = "invalidAccessToken";
const String INVALID_CLIENT = "invalidClient";
const String VALID_UID = "validUid";
const String VALID_ACCESS_TOKEN = "validAccessToken";
const String VALID_CLIENT = "validClient";

class EnterprisesRepositoryMock implements IEnterpriseRepository {
  @override
  Future<SearchResponseModel> search(SearchModel searchModel) async {
    SearchResponseModel searchResponseModel;
    if (searchModel.authModel.uid != VALID_UID ||
        searchModel.authModel.client != VALID_CLIENT ||
        searchModel.authModel.accessToken != VALID_ACCESS_TOKEN) {
      searchResponseModel = SearchResponseModel(enterprises: null);
    } else if (searchModel.search.isEmpty) {
      return SearchResponseModel(enterprises: []);
    } else {
      searchResponseModel = SearchResponseModel(enterprises: [
        new EnterpriseModel(
            enterpriseName: "enterprise 1 name",
            description: "enterprise 1 description"),
        new EnterpriseModel(
            enterpriseName: "enterprise 2 name",
            description: "enterprise 2 description"),
      ]);
    }
    await Future.delayed(Duration(seconds: 1));
    return searchResponseModel;
  }

  @override
  Future<AuthModel> signin(LoginModel loginModel) {
    throw UnimplementedError();
  }
}

main() {
  initModule(
    AppModule(),
    changeBinds: [
      Bind<IEnterpriseRepository>((i) => EnterprisesRepositoryMock()),
    ],
  );

  EnterprisesViewModel enterprisesViewModel;
  SearchModel invalidSearchModel = SearchModel(
    search: "whatever",
    authModel: AuthModel(
      uid: INVALID_UID,
      client: VALID_CLIENT,
      accessToken: VALID_ACCESS_TOKEN,
    ),
  );
  SearchModel emptySearchTextSearchModel = SearchModel(
    search: "",
    authModel: AuthModel(
      uid: VALID_UID,
      client: VALID_CLIENT,
      accessToken: VALID_ACCESS_TOKEN,
    ),
  );
  SearchModel validSearchModel = SearchModel(
    search: "whatever",
    authModel: AuthModel(
      uid: VALID_UID,
      client: VALID_CLIENT,
      accessToken: VALID_ACCESS_TOKEN,
    ),
  );
  EnterpriseModel enterpriseModel = EnterpriseModel(
      enterpriseName: "Enterprise name", description: "Enterprise description");

  setUp(() {
    enterprisesViewModel = Modular.get<EnterprisesViewModel>();
    enterprisesViewModel.isSearchedOutput
        .listen(enterprisesViewModel.enterprisesStore.setEnterprisesModels);
  });

  group("Enterprises View Model Test:", () {
    test("init selected enterprise is null", () async {
      final enterprisesStore = enterprisesViewModel.enterprisesStore;
      expect(enterprisesStore.selectedEnterpriseModel, isNull);
    });
    test("selected enterprise is not null after select a enterprise", () async {
      enterprisesViewModel.selectEnterprise(enterpriseModel);
      final enterprisesStore = enterprisesViewModel.enterprisesStore;
      expect(enterprisesStore.selectedEnterpriseModel, isA<EnterpriseModel>());
    });
    test("selected enterprise null after select a null enterprise", () async {
      enterprisesViewModel.selectEnterprise(enterpriseModel);
      enterprisesViewModel.selectEnterprise(null);
      final enterprisesStore = enterprisesViewModel.enterprisesStore;
      expect(enterprisesStore.selectedEnterpriseModel, isNull);
    });
    test("init enterprises is null", () async {
      final enterprisesStore = enterprisesViewModel.enterprisesStore;
      expect(enterprisesStore.enterprisesModels, isNull);
    });
    test("enterprises is null after search with invalid uid", () async {
      enterprisesViewModel.isSearchedInput.add(invalidSearchModel);
      await Future.delayed(Duration(seconds: 2));
      expect(enterprisesViewModel.enterprisesStore.enterprisesModels, isNull);
    });
    test("enterprises is empty after search with empty search", () async {
      enterprisesViewModel.isSearchedInput.add(emptySearchTextSearchModel);
      await Future.delayed(Duration(seconds: 2));
      final enterprisesStore = enterprisesViewModel.enterprisesStore;
      expect(enterprisesStore.enterprisesModels, isA<List<EnterpriseModel>>());
      expect(enterprisesStore.enterprisesModels, isEmpty);
    });
    test("enterprises is not empty after valid search", () async {
      enterprisesViewModel.isSearchedInput.add(validSearchModel);
      await Future.delayed(Duration(seconds: 2));
      final enterprisesStore = enterprisesViewModel.enterprisesStore;
      expect(enterprisesStore.enterprisesModels, isA<List<EnterpriseModel>>());
      expect(enterprisesStore.enterprisesModels, isNotEmpty);
    });
  });
}
