import 'dart:async';
import 'package:app/interfaces/enterprises_respository_interface.dart';
import 'package:app/models/auth_model.dart';
import 'package:app/models/login_model.dart';
import 'package:app/stores/login_store.dart';

class LoginViewModel {
  final LoginStore loginStore;
  final IEnterpriseRepository enterpriseRepository;
  final _isAuthenticating = StreamController<bool>.broadcast();
  final _isInvalidCredentials = StreamController<bool>.broadcast();
  final _isAuthenticated = StreamController<LoginModel>.broadcast();

  LoginViewModel(this.loginStore, this.enterpriseRepository);

  Sink<bool> get isAuthenticatingInput => _isAuthenticating.sink;

  Sink<bool> get isInvalidCredentialsInput => _isInvalidCredentials.sink;

  Sink<LoginModel> get isAuthenticatedInput => _isAuthenticated.sink;

  Stream<bool> get isAuthenticatingOutput => _isAuthenticating.stream;

  Stream<bool> get isInvalidCredentialsOutput => _isInvalidCredentials.stream;

  Stream<AuthModel> get isAuthenticatedOutput =>
      _isAuthenticated.stream.asyncMap(login);

  Future<AuthModel> login(LoginModel loginModel) async {
    isAuthenticatingInput.add(true);
    isInvalidCredentialsInput.add(false);
    AuthModel authModel = await enterpriseRepository.signin(loginModel);
    isAuthenticatingInput.add(false);
    if (authModel == null) isInvalidCredentialsInput.add(true);
    return authModel;
  }

  void dispose() {
    _isAuthenticating.close();
    _isInvalidCredentials.close();
    _isAuthenticated.close();
  }
}
