import 'dart:async';

import 'package:app/interfaces/enterprises_respository_interface.dart';
import 'package:app/models/enterprise_model.dart';
import 'package:app/models/search_model.dart';
import 'package:app/models/search_response_model.dart';
import 'package:app/stores/enterprises_store.dart';

class EnterprisesViewModel {
  final IEnterpriseRepository enterpriseRepository;
  final EnterprisesStore enterprisesStore;
  final _isSearching = StreamController<bool>.broadcast();
  final _isSearched = StreamController<SearchModel>.broadcast();

  EnterprisesViewModel(this.enterprisesStore, this.enterpriseRepository);

  Sink<bool> get isSearchingInput => _isSearching.sink;

  Sink<SearchModel> get isSearchedInput => _isSearched.sink;

  Stream<bool> get isSearchingOutput => _isSearching.stream;

  Stream<List<EnterpriseModel>> get isSearchedOutput =>
      _isSearched.stream.asyncMap(search);

  Future<List<EnterpriseModel>> search(SearchModel searchModel) async {
    enterprisesStore.setEnterprisesModels(null);
    isSearchingInput.add(true);
    SearchResponseModel searchResponse =
        await enterpriseRepository.search(searchModel);
    isSearchingInput.add(false);
    return searchResponse.enterprises;
  }

  void selectEnterprise(EnterpriseModel enterpriseModel) {
    enterprisesStore.setSelectedEnterpriseModel(enterpriseModel);
  }

  void dispose() {
    _isSearching.close();
    _isSearched.close();
  }
}
