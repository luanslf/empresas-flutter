import 'package:app/interfaces/http_client_service_interface.dart';
import 'package:dio/dio.dart';

class DioHttpClientService implements IHttpClientService {
  Dio dio = Dio();

  @override
  Future get(String url, {Map<String, String> headers}) async {
    Map response;
    try {
      Response httpResponse =
          await dio.get(url, options: Options(headers: headers));
      response = Map();
      response.addAll(httpResponse.data as Map);
    } on DioError catch (e) {
      print("DioError: $e");
    } catch (e) {
      print("Exception: $e");
    }
    return response;
  }

  @override
  Future<Map> post(String url, body, {Map<String, String> headers}) async {
    Map response;
    try {
      Response httpResponse =
          await dio.post(url, data: body, options: Options(headers: headers));
      response = Map();
      response.addAll(httpResponse.data as Map);
      response.addAll(httpResponse.headers.map);
    } on DioError catch (e) {
      print("DioError: $e");
    } catch (e) {
      print("Exception: $e");
    }
    return response;
  }
}
