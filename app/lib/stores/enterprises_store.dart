import 'package:app/models/enterprise_model.dart';
import 'package:mobx/mobx.dart';
part 'enterprises_store.g.dart';

class EnterprisesStore = _EnterprisesStore with _$EnterprisesStore;

abstract class _EnterprisesStore with Store {
  @observable
  EnterpriseModel selectedEnterpriseModel;

  @observable
  List<EnterpriseModel> enterprisesModels;

  @action
  void setSelectedEnterpriseModel(EnterpriseModel enterpriseModel) {
    this.selectedEnterpriseModel = enterpriseModel;
  }

  @action
  void setEnterprisesModels(List<EnterpriseModel> enterprises) {
    this.enterprisesModels = enterprises;
  }

  @computed
  int get enterprisesQuantity => enterprisesModels.length;
}
