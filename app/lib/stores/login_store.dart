import 'package:app/models/auth_model.dart';
import 'package:mobx/mobx.dart';

part 'login_store.g.dart';

class LoginStore = _LoginStore with _$LoginStore;

abstract class _LoginStore with Store {
  @observable
  AuthModel authModel;

  @action
  void setAuthModel(AuthModel authModel) {
    this.authModel = authModel;
  }

  @computed
  bool get isLogged => this.authModel != null;
}
