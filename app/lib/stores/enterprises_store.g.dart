// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'enterprises_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$EnterprisesStore on _EnterprisesStore, Store {
  Computed<int> _$enterprisesQuantityComputed;

  @override
  int get enterprisesQuantity => (_$enterprisesQuantityComputed ??=
          Computed<int>(() => super.enterprisesQuantity,
              name: '_EnterprisesStore.enterprisesQuantity'))
      .value;

  final _$selectedEnterpriseModelAtom =
      Atom(name: '_EnterprisesStore.selectedEnterpriseModel');

  @override
  EnterpriseModel get selectedEnterpriseModel {
    _$selectedEnterpriseModelAtom.reportRead();
    return super.selectedEnterpriseModel;
  }

  @override
  set selectedEnterpriseModel(EnterpriseModel value) {
    _$selectedEnterpriseModelAtom
        .reportWrite(value, super.selectedEnterpriseModel, () {
      super.selectedEnterpriseModel = value;
    });
  }

  final _$enterprisesModelsAtom =
      Atom(name: '_EnterprisesStore.enterprisesModels');

  @override
  List<EnterpriseModel> get enterprisesModels {
    _$enterprisesModelsAtom.reportRead();
    return super.enterprisesModels;
  }

  @override
  set enterprisesModels(List<EnterpriseModel> value) {
    _$enterprisesModelsAtom.reportWrite(value, super.enterprisesModels, () {
      super.enterprisesModels = value;
    });
  }

  final _$_EnterprisesStoreActionController =
      ActionController(name: '_EnterprisesStore');

  @override
  void setSelectedEnterpriseModel(EnterpriseModel enterpriseModel) {
    final _$actionInfo = _$_EnterprisesStoreActionController.startAction(
        name: '_EnterprisesStore.setSelectedEnterpriseModel');
    try {
      return super.setSelectedEnterpriseModel(enterpriseModel);
    } finally {
      _$_EnterprisesStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setEnterprisesModels(List<EnterpriseModel> enterprises) {
    final _$actionInfo = _$_EnterprisesStoreActionController.startAction(
        name: '_EnterprisesStore.setEnterprisesModels');
    try {
      return super.setEnterprisesModels(enterprises);
    } finally {
      _$_EnterprisesStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
selectedEnterpriseModel: ${selectedEnterpriseModel},
enterprisesModels: ${enterprisesModels},
enterprisesQuantity: ${enterprisesQuantity}
    ''';
  }
}
