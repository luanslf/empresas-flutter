import 'dart:convert';

import 'package:app/interfaces/enterprises_respository_interface.dart';
import 'package:app/interfaces/http_client_service_interface.dart';
import 'package:app/models/auth_model.dart';
import 'package:app/models/login_model.dart';
import 'package:app/models/search_response_model.dart';
import 'package:app/models/search_model.dart';

class IoasysEnterpriseRepository implements IEnterpriseRepository {
  final IHttpClientService httpClientService;
  final String signinUrl =
      "https://empresas.ioasys.com.br/api/v1/users/auth/sign_in";

  final String searchUrl =
      "https://empresas.ioasys.com.br/api/v1/enterprises?name=";

  IoasysEnterpriseRepository(this.httpClientService);

  @override
  Future<AuthModel> signin(LoginModel loginModel) async {
    AuthModel authModel;
    Map<String, String> body = {
      "email": loginModel.email,
      "password": loginModel.password
    };
    try {
      Map json = await httpClientService.post(signinUrl, body);
      //print("json: $json");
      if (json != null) authModel = AuthModel.fromJson(json);
      //print("authModel: $authModel");
    } catch (e) {
      print(e);
    }
    return authModel;
  }

  @override
  Future<SearchResponseModel> search(SearchModel searchModel) async {
    SearchResponseModel searchResponse;
    Map<String, String> headers = {
      "access-token": searchModel.authModel.accessToken,
      "uid": searchModel.authModel.uid,
      "client": searchModel.authModel.client
    };
    String url = searchUrl + searchModel.search;
    try {
      Map json = await httpClientService.get(url, headers: headers);
      searchResponse = SearchResponseModel.fromJson(json);
    } catch (e) {
      searchResponse = SearchResponseModel(enterprises: []);
    }
    return searchResponse;
  }
}
