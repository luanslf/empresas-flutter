import 'package:flutter/material.dart';

class LoadingLoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      color: Colors.black45,
      alignment: Alignment.center,
      child: CircularProgressIndicator(),
    );
  }
}
