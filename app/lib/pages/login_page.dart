import 'package:app/controlles/app_controller.dart';
import 'package:app/helpers/login_validator.dart';
import 'package:app/models/login_model.dart';
import 'package:app/pages/components/login_button.dart';
import 'package:app/pages/components/login_text_field.dart';
import 'package:app/pages/components/sliver_login_page_header.dart';
import 'package:app/view_models/login_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'components/invalid_credentials_message.dart';
import 'loading_login_page.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  LoginModel loginModel = LoginModel();
  LoginViewModel loginViewModel = Modular.get<AppController>().loginViewModel;
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    loginViewModel.isAuthenticatedOutput
        .listen(loginViewModel.loginStore.setAuthModel);
  }

  @override
  void dispose() {
    loginViewModel.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: StreamBuilder<bool>(
          initialData: false,
          stream: loginViewModel.isAuthenticatingOutput,
          builder: (_, snapshot) {
            bool isAuthenticating = snapshot.data;
            return Stack(
              fit: StackFit.expand,
              children: [
                CustomScrollView(
                  slivers: [
                    SliverPersistentHeader(
                      pinned: true,
                      delegate: SliverLoginPageHeader(
                        expandedHeight: MediaQuery.of(context).size.height / 4,
                      ),
                    ),
                    SliverToBoxAdapter(
                      child: Container(
                        margin: EdgeInsets.only(top: 10.0),
                        padding: EdgeInsets.symmetric(horizontal: 20.0),
                        child: StreamBuilder<bool>(
                          initialData: false,
                          stream: loginViewModel.isInvalidCredentialsOutput,
                          builder: (_, snapshot) {
                            bool isInvalidCredentials = snapshot.data;
                            return Form(
                              key: formKey,
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  LoginTextField(
                                    obscureText: false,
                                    label: "Email",
                                    hasError: isInvalidCredentials,
                                    validator: emailTextFieldValidator,
                                    onSaved: emailTextFieldOnSaved,
                                  ),
                                  LoginTextField(
                                    obscureText: true,
                                    label: "Senha",
                                    hasError: isInvalidCredentials,
                                    validator: passwordTextFieldValidator,
                                    onSaved: passwordTextFieldOnSaved,
                                  ),
                                  isInvalidCredentials
                                      ? InvalidCredentialsMessage()
                                      : Container(),
                                  Container(
                                    margin:
                                        EdgeInsets.symmetric(vertical: 20.0),
                                    child: LoginButton(
                                      onPressed: handleLoginButtonTap,
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                        ),
                      ),
                    ),
                  ],
                ),
                isAuthenticating ? LoadingLoginPage() : Container(),
              ],
            );
          },
        ),
      ),
    );
  }

  bool validateForm() {
    return formKey.currentState.validate();
  }

  void saveForm() {
    return formKey.currentState.save();
  }

  String emailTextFieldValidator(String email) {
    if (LoginValidator.validateEmail(email)) return null;
    return "Email inválido";
  }

  String passwordTextFieldValidator(String password) {
    if (LoginValidator.validatePassword(password)) return null;
    return "Senha inválida";
  }

  void emailTextFieldOnSaved(String email) {
    loginModel.email = email;
  }

  void passwordTextFieldOnSaved(String password) {
    loginModel.password = password;
  }

  Future<void> handleLoginButtonTap() async {
    if (validateForm()) {
      saveForm();
      loginViewModel.isAuthenticatedInput.add(loginModel);
    }
  }
}
