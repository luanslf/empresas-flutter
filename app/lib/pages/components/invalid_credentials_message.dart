import 'package:flutter/material.dart';

class InvalidCredentialsMessage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10.0),
      child: Text(
        "Credenciais inválidas",
        textAlign: TextAlign.right,
        style: TextStyle(color: Colors.red),
      ),
    );
  }
}
