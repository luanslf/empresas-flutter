import 'package:flutter/material.dart';

class AppBarBackButton extends StatelessWidget {
  final Function onTap;

  AppBarBackButton({@required this.onTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 6.0, horizontal: 8.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5.0),
        color: Colors.black12,
      ),
      child: InkWell(
        onTap: onTap,
        child: Icon(Icons.arrow_back),
      ),
    );
  }
}
