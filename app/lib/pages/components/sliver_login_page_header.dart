import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';

class SliverLoginPageHeader extends SliverPersistentHeaderDelegate {
  final double expandedHeight;
  final Color appBarBackgroundColor = Color(0xFFE01E69);

  SliverLoginPageHeader({@required this.expandedHeight});

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return ClipPath(
      clipper: OvalBottomBorderClipper(),
      child: Stack(
        fit: StackFit.expand,
        overflow: Overflow.visible,
        children: [
          Container(color: appBarBackgroundColor),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(top: 10.0),
                  child: Image.asset("images/logo.png"),
                ),
              ),
              Expanded(
                child: Opacity(
                  opacity: (1 - shrinkOffset / expandedHeight),
                  child: Padding(
                    padding: EdgeInsets.only(top: 5.0),
                    child: Text(
                      "Seja bem vindo ao empresas!",
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  double get maxExtent => expandedHeight;

  @override
  double get minExtent => expandedHeight / 2;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) => true;
}
