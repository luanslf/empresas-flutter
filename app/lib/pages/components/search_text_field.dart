import 'package:flutter/material.dart';

class SearchTextField extends StatefulWidget {
  final bool enabled;
  final Function onSubmitted;
  final Function(String) onSaved;

  SearchTextField({
    @required this.enabled,
    @required this.onSubmitted,
    @required this.onSaved,
  });

  @override
  _SearchTextFieldState createState() => _SearchTextFieldState();
}

class _SearchTextFieldState extends State<SearchTextField> {
  TextEditingController controller = TextEditingController();
  ValueNotifier<bool> enabledSubmitIconListenable = ValueNotifier<bool>(false);

  @override
  void initState() {
    super.initState();
    controller.addListener(() {
      String text = controller.text;
      enabledSubmitIconListenable.value = text.isNotEmpty;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5.0,
      margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 9.0),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 10.0),
        child: TextFormField(
          controller: controller,
          enabled: widget.enabled,
          onSaved: widget.onSaved,
          style: Theme.of(context).textTheme.bodyText1,
          decoration: InputDecoration(
            hintText: "Pesquise por empresa",
            enabledBorder: InputBorder.none,
            focusedBorder: InputBorder.none,
            suffixIcon: ValueListenableBuilder<bool>(
              valueListenable: enabledSubmitIconListenable,
              builder: (_, enabledIcon, __) {
                return IconButton(
                  icon: Icon(Icons.send),
                  onPressed: enabledIcon ? widget.onSubmitted : null,
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
