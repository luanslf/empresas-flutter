import 'package:app/models/enterprise_model.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;

class EnterpriseListItem extends StatelessWidget {
  final EnterpriseModel enterpriseModel;
  final Function(EnterpriseModel) onTap;

  EnterpriseListItem({@required this.enterpriseModel, @required this.onTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 15.0, right: 15.0, bottom: 15.0),
      child: InkWell(
        onTap: () => onTap(enterpriseModel),
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 30.0, horizontal: 10.0),
          decoration: BoxDecoration(
            color: Color((math.Random().nextDouble() * 0xFFFFFF).toInt())
                .withOpacity(1.0),
            borderRadius: BorderRadius.circular(5.0),
          ),
          child: Text(
            enterpriseModel.enterpriseName,
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.headline5,
          ),
        ),
      ),
    );
  }
}
