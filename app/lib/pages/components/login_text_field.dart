import 'package:flutter/material.dart';

class LoginTextField extends StatefulWidget {
  final bool obscureText;
  final String label;
  final bool hasError;
  final String Function(String) validator;
  final void Function(String) onSaved;

  LoginTextField({
    @required this.obscureText,
    @required this.label,
    @required this.hasError,
    @required this.validator,
    @required this.onSaved,
  });

  @override
  _LoginTextFieldState createState() => _LoginTextFieldState();
}

class _LoginTextFieldState extends State<LoginTextField> {
  bool isObscure = true;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Container(
          margin: EdgeInsets.symmetric(vertical: 10.0),
          child: Text(
            widget.label,
            textAlign: TextAlign.start,
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 10.0),
          decoration: BoxDecoration(
            color: Color(0xFFF0F0F0),
            borderRadius: BorderRadius.circular(5.0),
            border: Border.all(
              color: widget.hasError ? Colors.red : Colors.transparent,
            ),
          ),
          child: TextFormField(
            onSaved: widget.onSaved,
            validator: widget.validator,
            obscureText: widget.obscureText ? isObscure : widget.obscureText,
            style: TextStyle(color: Colors.black),
            decoration: InputDecoration(
              border: InputBorder.none,
              suffixIcon: widget.hasError
                  ? Icon(Icons.error, color: Colors.red)
                  : widget.obscureText
                      ? InkWell(
                          onTap: () {
                            setState(() {
                              isObscure = !isObscure;
                            });
                          },
                          child: Icon(isObscure
                              ? Icons.visibility
                              : Icons.visibility_off),
                        )
                      : null,
            ),
          ),
        ),
      ],
    );
  }
}
