import 'package:app/controlles/app_controller.dart';
import 'package:app/models/enterprise_model.dart';
import 'package:app/models/search_model.dart';
import 'package:app/pages/components/search_text_field.dart';
import 'package:app/stores/enterprises_store.dart';
import 'package:app/view_models/enterprises_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'components/enterprise_list_item.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  SearchModel searchModel;
  EnterprisesViewModel enterprisesViewModel =
      Modular.get<AppController>().enterprisesViewModel;
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    final auth =
        Modular.get<AppController>().loginViewModel.loginStore.authModel;
    searchModel = SearchModel(authModel: auth);
    enterprisesViewModel.isSearchedOutput
        .listen(enterprisesViewModel.enterprisesStore.setEnterprisesModels);
  }

  @override
  Widget build(BuildContext context) {
    double expandedHeight = MediaQuery.of(context).size.height / 3.5;
    double toolbarHeight = 0.6 * expandedHeight;
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: CustomScrollView(
          slivers: [
            StreamBuilder<bool>(
              initialData: false,
              stream: enterprisesViewModel.isSearchingOutput,
              builder: (_, snapshot) {
                bool searching = snapshot.data;
                return SliverAppBar(
                  pinned: true,
                  elevation: 0.0,
                  titleSpacing: 0.0,
                  toolbarHeight: toolbarHeight,
                  expandedHeight: expandedHeight,
                  backgroundColor: Color(0xFFAD1E98),
                  flexibleSpace: FlexibleSpaceBar(
                    centerTitle: true,
                    titlePadding: EdgeInsets.all(0.0),
                    background: Container(color: Color(0xFFAD1E98)),
                    title: Form(
                      key: formKey,
                      child: SearchTextField(
                        enabled: !searching,
                        onSaved: searchTextFieldOnSaved,
                        onSubmitted: searchTextFieldOnSubmitted,
                      ),
                    ),
                  ),
                );
              },
            ),
            StreamBuilder<bool>(
              initialData: false,
              stream: enterprisesViewModel.isSearchingOutput,
              builder: (_, snapshot) {
                bool searching = snapshot.data;
                if (!searching) return SliverToBoxAdapter(child: Container());
                return SliverFillRemaining(
                  child: Container(
                    alignment: Alignment.center,
                    child: CircularProgressIndicator(),
                  ),
                );
              },
            ),
            Observer(
              builder: (_) {
                EnterprisesStore enterprisesStore =
                    enterprisesViewModel.enterprisesStore;
                if (enterprisesStore.enterprisesModels == null) {
                  return SliverToBoxAdapter(child: Container());
                }
                return SliverAppBar(
                  pinned: true,
                  elevation: 0.0,
                  centerTitle: false,
                  title: Text(
                    '${enterprisesStore.enterprisesQuantity} resultados encontrados',
                    textAlign: TextAlign.start,
                    style: Theme.of(context).textTheme.bodyText2,
                  ),
                );
              },
            ),
            Observer(builder: (_) {
              EnterprisesStore enterprisesStore =
                  enterprisesViewModel.enterprisesStore;
              if (enterprisesStore.enterprisesModels == null) {
                return SliverToBoxAdapter(child: Container());
              }
              return SliverList(
                delegate: SliverChildListDelegate(
                  enterprisesStore.enterprisesModels
                      .map(
                        (e) => EnterpriseListItem(
                          enterpriseModel: e,
                          onTap: selectEnterprise,
                        ),
                      )
                      .toList(),
                ),
              );
            }),
          ],
        ),
      ),
    );
  }

  void searchTextFieldOnSubmitted() {
    formKey.currentState.save();
    enterprisesViewModel.isSearchedInput.add(searchModel);
  }

  void searchTextFieldOnSaved(String search) {
    searchModel.search = search;
  }

  void selectEnterprise(EnterpriseModel enterpriseModel) {
    enterprisesViewModel.selectEnterprise(enterpriseModel);
  }
}
