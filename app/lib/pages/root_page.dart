import 'package:app/controlles/app_controller.dart';
import 'package:app/pages/entrerprise_page.dart';
import 'package:app/pages/home_page.dart';
import 'package:app/pages/login_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

class RootPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Observer(builder: (_) {
      Widget screen;
      final app = Modular.get<AppController>();
      final loginStore = app.loginViewModel.loginStore;
      final enterprisesStore = app.enterprisesViewModel.enterprisesStore;
      if (loginStore.authModel == null) {
        screen = LoginPage();
      } else if (enterprisesStore.selectedEnterpriseModel == null) {
        screen = HomePage();
      } else {
        screen = EnterprisePage();
      }
      return screen;
    });
  }
}
