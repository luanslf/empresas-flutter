import 'package:app/controlles/app_controller.dart';
import 'package:app/view_models/enterprises_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'components/app_bar_back_button.dart';

class EnterprisePage extends StatefulWidget {
  @override
  _EnterprisePageState createState() => _EnterprisePageState();
}

class _EnterprisePageState extends State<EnterprisePage> {
  EnterprisesViewModel enterprisesViewModel =
      Modular.get<AppController>().enterprisesViewModel;

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (_) {
        final selectedEnterpriseModel =
            enterprisesViewModel.enterprisesStore.selectedEnterpriseModel;
        if (selectedEnterpriseModel == null) return Container();
        return Scaffold(
          appBar: AppBar(
            automaticallyImplyLeading: false,
            leading: AppBarBackButton(onTap: handleAppBarBackButtonTap),
            title: Text(selectedEnterpriseModel.enterpriseName),
          ),
          body: ListView(
            shrinkWrap: true,
            children: [
              Container(
                color: Colors.blue,
                padding: EdgeInsets.symmetric(vertical: 45.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      selectedEnterpriseModel.enterpriseName,
                      style: Theme.of(context).textTheme.headline5,
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 15.0),
                child: Text(
                  selectedEnterpriseModel.description,
                  textAlign: TextAlign.justify,
                  style: Theme.of(context).textTheme.headline6,
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  void handleAppBarBackButtonTap() {
    enterprisesViewModel.selectEnterprise(null);
  }
}
