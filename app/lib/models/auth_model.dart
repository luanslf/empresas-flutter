class AuthModel {
  String uid;
  String client;
  String accessToken;

  AuthModel({this.uid, this.client, this.accessToken});

  AuthModel.fromJson(Map json) {
    uid = (json['uid'] as List).first;
    client = (json['client'] as List).first;
    accessToken = (json['access-token'] as List).first;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['uid'] = this.uid;
    data['client'] = this.client;
    data['access-token'] = this.accessToken;
    return data;
  }

  @override
  String toString() =>
      "AuthModel(uid: $uid - client: $client - accessToken: $accessToken)";
}
