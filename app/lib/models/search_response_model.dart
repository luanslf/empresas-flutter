import 'package:app/models/enterprise_model.dart';

class SearchResponseModel {
  List<EnterpriseModel> enterprises;

  SearchResponseModel({this.enterprises});

  SearchResponseModel.fromJson(Map<dynamic, dynamic> json) {
    if (json['enterprises'] != null) {
      enterprises = new List<EnterpriseModel>();
      json['enterprises'].forEach((v) {
        enterprises.add(new EnterpriseModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.enterprises != null) {
      data['enterprises'] = this.enterprises.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
