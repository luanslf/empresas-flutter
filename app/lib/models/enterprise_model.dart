class EnterpriseModel {
  int id;
  String enterpriseName;
  String description;
  String photo;

  EnterpriseModel({this.id, this.enterpriseName, this.description, this.photo});

  EnterpriseModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    enterpriseName = json['enterprise_name'];
    description = json['description'];
    photo = json['photo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['enterprise_name'] = this.enterpriseName;
    data['description'] = this.description;
    data['photo'] = this.photo;
    return data;
  }
}
