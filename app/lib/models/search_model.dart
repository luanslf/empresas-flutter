import 'package:app/models/auth_model.dart';

class SearchModel {
  String search;
  AuthModel authModel;

  SearchModel({this.search, this.authModel});

  @override
  String toString() =>
      "SearchModel(search: $search - authModel: ${authModel.toString()})";
}
