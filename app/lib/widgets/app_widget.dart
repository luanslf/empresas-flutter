import 'package:app/pages/root_page.dart';
import 'package:flutter/material.dart';

class AppWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Empresas Flutter',
      theme: ThemeData(
        buttonColor: Color(0xFFE01E69),
        textTheme: TextTheme(
          button: TextStyle(color: Colors.white),
          headline5: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
          subtitle1: TextStyle(
            color: Colors.white,
          ),
          bodyText2: TextStyle(
            color: Colors.black87.withOpacity(0.6),
          ),
        ),
        appBarTheme: AppBarTheme(
          elevation: 0.0,
          centerTitle: true,
          color: Colors.white,
          textTheme: TextTheme(
            headline6: TextStyle(
              color: Colors.black,
              fontSize: 20.0,
              fontWeight: FontWeight.bold,
            ),
          ),
          iconTheme: IconThemeData(
            color: Color(0xFFE01E69),
          ),
        ),
        accentColor: Color(0xFFE01E69),
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: RootPage(),
      debugShowCheckedModeBanner: false,
    );
  }
}
