import 'package:app/controlles/app_controller.dart';
import 'package:app/interfaces/enterprises_respository_interface.dart';
import 'package:app/interfaces/http_client_service_interface.dart';
import 'package:app/repositories/ioasys_enterprise_repository.dart';
import 'package:app/services/dio_http_client_service.dart';
import 'package:app/stores/enterprises_store.dart';
import 'package:app/stores/login_store.dart';
import 'package:app/view_models/enterprises_view_model.dart';
import 'package:app/view_models/login_view_model.dart';
import 'package:app/widgets/app_widget.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_modular/flutter_modular.dart';

class AppModule extends MainModule {
  @override
  List<Bind> get binds {
    return [
      Bind((i) => AppController(i.get(), i.get())),
      Bind((i) => LoginViewModel(i.get(), i.get())),
      Bind((i) => EnterprisesViewModel(i.get(), i.get())),
      Bind<IEnterpriseRepository>((i) => IoasysEnterpriseRepository(i.get())),
      Bind<IHttpClientService>((i) => DioHttpClientService()),
      Bind((i) => LoginStore()),
      Bind((i) => EnterprisesStore()),
    ];
  }

  @override
  Widget get bootstrap => AppWidget();

  @override
  List<ModularRouter> get routers => null;
}
