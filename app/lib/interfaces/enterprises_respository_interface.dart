import 'package:app/models/auth_model.dart';
import 'package:app/models/login_model.dart';
import 'package:app/models/search_model.dart';
import 'package:app/models/search_response_model.dart';

abstract class IEnterpriseRepository {
  Future<AuthModel> signin(LoginModel loginModel);
  Future<SearchResponseModel> search(SearchModel searchModel);
}
