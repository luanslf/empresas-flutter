abstract class IHttpClientService {
  Future get(String url, {Map<String, String> headers});
  Future post(String url, dynamic body, {Map<String, String> headers});
}
