import 'package:app/repositories/ioasys_enterprise_repository.dart';
import 'package:app/services/dio_http_client_service.dart';
import 'package:app/stores/enterprises_store.dart';
import 'package:app/stores/login_store.dart';
import 'package:app/view_models/enterprises_view_model.dart';
import 'package:app/view_models/login_view_model.dart';

class AppController {
  final LoginViewModel loginViewModel;
  final EnterprisesViewModel enterprisesViewModel;

  AppController(this.loginViewModel, this.enterprisesViewModel);

  /* static final AppController instance = AppController._();
  AppController._(); */

  /* LoginViewModel loginViewModel = LoginViewModel(
      LoginStore(), IoasysEnterpriseRepository(DioHttpClientService()));

  EnterprisesViewModel enterprisesViewModel = EnterprisesViewModel(
      EnterprisesStore(), IoasysEnterpriseRepository(DioHttpClientService())); */
}
